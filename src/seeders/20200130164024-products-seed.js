'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {


    queryInterface.bulkInsert('Products', [{

      name: 'Yogurt',
      photo: 'https://plazavea.vteximg.com.br/arquivos/ids/173145-450-450/1000777004.jpg?v=635785248066370000',
      price: 3.5,
    }], {});

    queryInterface.bulkInsert('Products', [{

      name: 'Queso',
      photo: 'https://img.vixdata.io/pd/jpg-large/es/sites/default/files/imj/entrepadres/b/beneficios-de-comer-queso-en-ninos-%201.jpg',
      price: 2.0,
    }], {});

    return queryInterface.bulkInsert('Products', [{

      name: 'Queso Laive',
      photo: 'https://vivanda.vteximg.com.br/arquivos/ids/178824-1000-1000/707121.jpg?v=636973633044300000',
      price: 2.5,
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Products', null, {});

  }
};
