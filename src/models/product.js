'use strict';

import Sequelize from 'sequelize';

module.exports = (sequelize, DataTypes) => {



  const Product = sequelize.define('Product', {


    name: DataTypes.STRING,
    photo: DataTypes.STRING,
    price: DataTypes.DOUBLE,

  }, {});



  Product.associate = function (models) {

    Product.belongsToMany(models.Order, {
      through: 'OrderProduct',
      as: 'orders',
      foreignKey: 'productId',
      otherKey: 'orderId'
    });


  };


  return Product;
};