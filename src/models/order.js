'use strict';
module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
    shipping_cost: DataTypes.DOUBLE,
    tax: DataTypes.DOUBLE

  }, {});
  Order.associate = function (models) {
    Order.belongsToMany(models.Product, {
      through: 'OrderProduct',
      as: 'products',
      foreignKey: 'orderId',
      otherKey: 'productId'
    });
  };
  return Order;
};