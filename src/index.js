
import express, { json } from 'express';

import morgan from 'morgan';


import ProductsRoutes from './routes/products';
import OrdersRoutes from './routes/orders';

const app = express();


//Middlewares
app.use(morgan('dev'));
app.use(json());


//Rutas
app.use('/api/products', ProductsRoutes);
app.use('/api/orders', OrdersRoutes);


//Conectando
app.listen(3000, () => {
  console.log('App listening on port 3000');
});