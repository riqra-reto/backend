import { Router } from 'express';
const router = Router();
import { search } from './../controllers/product.controller';

router.get('/', search);
export default router;