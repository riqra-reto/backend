import { Router } from 'express';
const router = Router();

//Funciones
import { addProduct } from './../controllers/order.controller';


router.post('/', addProduct);


export default router;