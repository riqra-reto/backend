import models from './../models';
import Product from './../models/product'



export async function addProduct(req, res) {

    let { products } = req.body;


    if (products == null || products.length == 0) {
        return res.status(400);
    }

    //Recorremos el products para crear el precio total 

    var priceTotal = 0.0;
    var shippingCost = 0.0;
    var taxesCost = 0.0;


    let transaction;

    try {


        transaction = await models.sequelize.transaction({ autocommit: false });

        //Creamos la orden

        const newOrder = await models.Order.create({
            shipping_cost: null,
            tax: null
        }, { w: 1 }, { returning: true });


        //Recorremos el carrito para verificar si existen y calcular montos

        console.log('Comenzando for await');

        for await (const item of products) {
            const product = await models.Product.findByPk(item.id);

            if (!product) {
                console.log('No se encontró')
                if (transaction) await transaction.rollback();
                return res.status(404);
            }

            //Agregamos

            await models.OrderProduct.create({
                productId: product.id,
                orderId: newOrder.id
            });

            //calculamos
            console.log(product.price, item.quantity);


            priceTotal += (product.price * item.quantity);
        }
        console.log('Finalizando for await', priceTotal);


        //Calcular costos
        shippingCost = 0.10 * priceTotal;
        taxesCost = 0.18 * priceTotal;

        console.log('Actualizamos ', {
            shipping_cost: shippingCost,
            tax: taxesCost
        }, newOrder.id);
        //actualizamos

        await newOrder.update({
            shipping_cost: shippingCost,
            tax: taxesCost
        });
        /*
                await models.Order.update({
                    shipping_cost: shippingCost,
                    tax: taxesCost
                }, { where: { id: newOrder.id } });*/


        return res.status(200).json({ success: true })

    } catch (error) {

        console.log('Error en Transaction', transaction, 'error es ', error);
        if (transaction) await transaction.rollback();


        return res.status(400).json('Error')
    }




}



