import models from './../models';
import Sequelize from 'sequelize';



export async function search(req, res) {

    console.log('Buscando ', req.query.name || '');
    
    const response = await models.Product.findAll({
        where: {
            name: {
                [Sequelize.Op.like]: `%${req.query.name || ''}%`
            }
        }
    });

    return res.json(response);


}